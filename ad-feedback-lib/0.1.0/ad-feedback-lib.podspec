#
# Be sure to run `pod lib lint ad-feedback-lib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ad-feedback-lib'
  s.version          = '0.1.0'
  s.summary          = 'ADFeedback-lib is used for bug tracking and is a open soure library'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "ADFeedback-lib is a clear, inclusive, modular, flexible, and consistent guide for design and development best practices"

  s.homepage         = 'https://github.com/Sagar/ad-feedback-lib'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Sagar' => 'adfeedback@gmail.com' }
  s.source           = { :git => 'https://sagar_xische@bitbucket.org/sagar_xische/ad-feedback-ios.git',  :branch => "master"}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '11.0'
  s.swift_version = "5.0"
  s.source_files = 'ADFeedback/**/*.{swift}'
  
  # s.resource_bundles = {
  #   'ad-feedback-lib' => ['ad-feedback-lib/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Alamofire'
end
